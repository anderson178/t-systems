package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author Денис Мироненко
 * @version $Id$
 * @since 13.9.2019
 */
public class Calculator {
    private Stack<Double> operands = new Stack<>();
    private Stack<String> operations = new Stack<>();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }
        List<String> temp = parsLine(statement);
        if (temp != null) {
            for (int i = 0; i < temp.size(); i++) {
                if (checkOperation(temp.get(i))) {
                    if (!operations.isEmpty()) {
                        String op = operations.pop();
                        if (getPriority(temp.get(i)) < getPriority(op)) {
                            double first = operands.pop();
                            double second = operands.pop();
                            operands.push(calculate(second, first, op));
                            operations.push(temp.get(i));
                            recursCalc();
                        } else if (getPriority(temp.get(i)) == getPriority(op)) {
                            operations.push(op);
                            operations.push(temp.get(i));
                            recursCalc();
                        } else {
                            operations.push(op);
                            operations.push(temp.get(i));
                        }
                    } else {
                        operations.push(temp.get(i));
                    }
                } else if (checkCloseParenthesis(temp.get(i).charAt(0)) || checkOpenParenthesis(temp.get(i).charAt(0))) {
                    operations.push(temp.get(i));
                } else {
                    operands.push(Double.valueOf(temp.get(i)));
                }
                if (i == temp.size() - 1) {
                    double first = operands.pop();
                    double second = operands.pop();
                    operands.push(calculate(second, first, operations.pop()));
                }
            }
            if (!operations.empty()) {
                double first = operands.pop();
                double second = operands.pop();
                operands.push(calculate(second, first, operations.pop()));
            }
        } else {
            return null;
        }
        return convertOutputValue(operands.pop());
    }

    /**
     * Ьethod convertы the result to the appropriate form
     *
     * @param value - штзге мфдгу
     * @return - type string the value
     */
    private String convertOutputValue(Double value) {
        String rst = "";
        String[] splitter = String.valueOf(value).split("\\.");
        if (splitter[1].length() == 1 && splitter[1].equals("0")) {
            rst = splitter[0];
        } else {
            rst = String.valueOf(value);
        }
        return rst;
    }

    /**
     * The method parses a string on separate elements
     *
     * @param line - input string
     * @return - list with separate elemants
     */
    private List<String> parsLine(String line) {
        List<String> rst = new ArrayList<>();
        char[] mas = line.toCharArray();
        StringBuilder temp = new StringBuilder();
        boolean checkTwoOperations = false;
        for (int i = 0; i < mas.length; i++) {
            if (checkOperation(mas[i])) {
                if (checkTwoOperations || (temp.toString().contains(".") && !checkPointer(temp.toString()))) {
                    return null;
                }
                rst.add(temp.toString());
                rst.add(Character.toString(mas[i]));
                temp.setLength(0);
                checkTwoOperations = true;
            } else {
                if (mas[i] != ',') {

                    if (checkOpenParenthesis(mas[i]) || checkCloseParenthesis(mas[i])) {
                        rst.add(Character.toString(mas[i]));
                        temp.setLength(0);
                        checkTwoOperations = false;
                    } else {
                        temp.append(mas[i]);
                        checkTwoOperations = false;
                    }
                } else {
                    return null;
                }
            }
        }
        rst.add(temp.toString());
        rst.remove("");
        return rst;
    }

    /**
     * the method check parenthesis of the open
     *
     * @param symbol - input symbol
     * @return - return true if parenthesi equal the open otherwise the false
     */
    private boolean checkOpenParenthesis(char symbol) {
        return symbol == '(';
    }

    /**
     * the method check parenthesis of the close
     *
     * @param symbol - input symbol
     * @return - return true if parenthesi equal the close otherwise the false
     */
    private boolean checkCloseParenthesis(char symbol) {
        return symbol == ')';
    }

    /**
     * The method checks the correct location of the point and quantity
     *
     * @param line - inut string
     * @return - if the point is the first or last or their number is more than one then it will return
     * false otherwise the true
     */
    private boolean checkPointer(String line) {
        String[] temp = line.split("\\.");
        return temp.length == 2 && !temp[0].equals("");
    }

    /**
     * The method performs operations with operands until there is only one operation left on the stack
     */
    private void recursCalc() {
        String ee = operations.get(operations.size() - 1);
        if (operations.size() > 1) {
            String firstOp = operations.pop();
            String secondOp = operations.pop();
            double first = operands.pop();
            double second = operands.pop();
            operands.push(calculate(second, first, secondOp));
            operations.push(firstOp);
            recursCalc();
        }
    }

    /**
     * The method checks if the symbol is an arephmetic action
     *
     * @param symbol - input symbol
     * @return - if the symbol is an arithmetic action, it will return the true otherwise the false
     */
    private boolean checkOperation(String symbol) {
        return symbol.equals("-") || symbol.equals("+") || symbol.equals("*") || symbol.equals("/");
    }

    /**
     * The method checks if the symbol is an arephmetic action
     *
     * @param symbol - input symbol
     * @return - if the symbol is an arithmetic action, it will return the true otherwise the false
     */
    private boolean checkOperation(char symbol) {
        return symbol == '-' || symbol == '+' || symbol == '*' || symbol == '/';
    }

    /**
     * Method performs an arethmithic action with operands
     *
     * @param first     - first number
     * @param second    - second number
     * @param operation - operation
     * @return -    result of performing an arithmetic action
     */
    private Double calculate(double first, double second, String operation) {
        double rst = 0;
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        if (operation.equals("/") && second == 0) {
            return null;
        }
        try {
            rst = Double.valueOf(engine.eval(first + operation + second).toString());
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return rst;
    }

    /**
     * Method calculates the priority of arithmetic operations
     *
     * @param operation - input operation
     * @return if "+" or "-" then return number one otherwise number two
     */
    private int getPriority(String operation) {
        int rst;
        if (operation.equals("+") || operation.equals("-")) {
            rst = 1;
        } else {
            rst = 2;
        }
        return rst;
    }
}
