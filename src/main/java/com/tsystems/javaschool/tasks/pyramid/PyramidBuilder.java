package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Денис Мироненко
 * @version $Id$
 * @since 10.09.2019
 */
public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        List<Integer> temp ;
        try {
             temp = inputNumbers.stream()
                     .filter(PyramidBuilder::checkNotNull)
                     .sorted(Comparator.naturalOrder())
                     .collect(Collectors.toList());
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
        int rows = this.calcRows(temp);
        int[][] mas = new int[this.calcRows(temp)][this.calcColumns(rows)];
        return this.fillMatrix(mas, temp);
    }

    /**
     * Method for fill the matrix
     *
     * @param mas  - input array
     * @param list - input a list with numbers
     * @return - fill array
     */
    private int[][] fillMatrix(int[][] mas, List<Integer> list) {
        int indent = 0;
        int index = list.size() - 1;
        for (int i = mas.length - 1; i >= 0; i--) {
            boolean flag = true;
            for (int j = mas[i].length - 1 - indent; j >= indent; j--) {
                if (flag) {
                    mas[i][j] = list.get(index--);
                    flag = false;
                } else {
                    flag = true;
                }
            }
            indent++;
        }
        return mas;
    }

    /**
     * Method to calculate count a columns of the matrix
     *
     * @param rows - count rows
     * @return
     */
    private int calcColumns(int rows) {
        int columns = 1;
        for (int i = 0; i < rows - 1; i++) {
            columns = columns + 2;
        }
        return columns;
    }

    /**
     * Method to calculate count a rows of the matrix
     *
     * @param list - input list with numbers
     * @return - count rows
     */
    private int calcRows(List<Integer> list) {
        int size = list.size();
        int rows = 1;
        int count = 0;
        while (count < size) {
            count = count + rows;
            rows++;
        }
        if (size != count) {
            throw new CannotBuildPyramidException();
        }
        return rows - 1;
    }

    /**
     * The method check object for null
     *
     * @param object - input object
     * @return - return true if not null otherwise Exception
     */
    private static boolean checkNotNull(Integer object) {
        if (object == null) {
            throw new CannotBuildPyramidException();
        }
        return true;
    }


}
