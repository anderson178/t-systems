package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Денис Мироненко
 * @version $Id$
 * @since 11.09.2019
 */
public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        boolean rst = false;
        if (x == null || y == null) {
            throw new IllegalArgumentException("List contain the null");
        } else if (x.size() == 0) {
            return true;
        }
        List<String> first = (List<String>) x.stream().map(Object::toString).collect(Collectors.toList());
        List<String> second = (List<String>) y.stream().map(Object::toString).collect(Collectors.toList());
        if (first.size() <= second.size()) {
            List<String> temp = new ArrayList<>();
            int pointerIndex = 0;
            for (String symbolFirst : first) {
                char[] masSymbolsFirst = symbolFirst.toCharArray();
                for (int i = pointerIndex; i < second.size(); i++) {
                    pointerIndex++;
                    char[] masSymbolsSecond = second.get(i).toCharArray();
                    if (compare(masSymbolsFirst, masSymbolsSecond)) {
                        temp.add(second.get(i));
                        break;
                    }
                }
                if (first.size() == temp.size()) {
                    rst = true;
                    break;
                }
            }
        }
        return rst;
    }

    /**
     * The method checks if two arrays are equal to each other
     *
     * @param masSymbolsFirst  - first array
     * @param masSymbolsSecond - second array
     * @return - if arrays are equal then return true otherwise false
     */
    private boolean compare(char[] masSymbolsFirst, char[] masSymbolsSecond) {
        boolean rst = false;
        if (masSymbolsFirst.length == masSymbolsSecond.length) {
            int count = 0;
            for (int i = 0; i < masSymbolsFirst.length; i++) {
                if (masSymbolsFirst[i] == masSymbolsSecond[i]) {
                    count++;
                } else {
                    break;
                }
            }
            if (count == masSymbolsFirst.length) {
                rst = true;
            }
        }
        return rst;
    }
}
